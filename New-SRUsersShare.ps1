<#
.SYNOPSIS
    Skrypt do tworzenia udzia��w dla u�ytkownik�w na podstawie nazwy konta.

.DESCRIPTION
    Problem: Utworzy� dla wszystkich u�ytkownik�w z AD udzia� kt�rego nazwa b�dzie odpowiada� nazwie konta u�ytkownika.
    Skrypt pobiera nazw� konta u�ytkownika tworzy katalog odpowiadaj�cy tej nazwie, udost�pnia i nadaje uprawnienia.
    Dla konta 'jan.kowalski' utworzony zostanie katalog 'jan.kowalski' z uprawnieniami: 
    NTFS - jan.kowalski/FullControl, SYSTEM/FullControl (ewentualnie Administrator/Read)
    Share - jan.kowalski/FullControl

    Ka�demu u�ytkownikowi mo�na podpi�c jego uzia� za pomoc� GPO: User Configuration -> Preferences -> Drive Maps -> New Mapped Drive, w pole Location
    podajemy \\serwer\�cie�ka do kotalogu z udzia�ami\%LogonUser% 


.PARAMETER OUPath
    �cie�ka do jednostki organizacyjnej AD w kt�rej znajduj� si� konta u�ytkownik�w (OU mog� by� zagnie�dzone). Parametr wymagany.

.PARAMETER Domain
    Nazwa domeny (Pierwszy cz�on nazwy). Parametr wymagany.

.PARAMETER DestPath
    �cie�ka do katalogu w kt�rym utworzone zostan� udzia�y. Parametr wymagany.

.PARAMETER Log
    �cie�ka do katalogu w kt�rym zapisany b�dzie log z wykonania skryptu. Parametr opcjonalny.

.PARAMETER Admin
    Parametr okre�laj�cy nazw� konta administratora, skrypt wywo�any z tym parametrem dodatkowo nada uprawnienia NTFS-ReadOnly
    dla podanego konta administratora. Parametr opcjonalny.

.INPUTS
    None.

.OUTPUTS
    Plik logu (New-SRUsersShare_2024-02-10_23.22.10.log) w formacie:

    2024-02-10_23.22.10 [INFO] > Log started
    2024-02-10_23.22.31 [SUCCESS] > [jan.kowalski] Creating directory.
    2024-02-10_23.22.31 [INFO] > [jan.kowalski] Share alredy exists.
    2024-02-10_23.22.31 [SUCCESS] > [anna.nowak] Creating directory.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Creating share and add permission.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Remove share permissions for a group: Everyone.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Disable NTFS inheritance.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Full Control for anna.nowak.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Full for SYSTEM.
    2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Read for Administrator.
    ...

.NOTES
    Version:        1.1
    Author:         Sebastian Cicho�ski
    Creation Date:  12.2023
    Projecturi:     https://gitlab.com/powershell1990849/new-srusersshare
  
.EXAMPLE
  New-SRUsersShare.ps1 -OUPath "OU=Users,OU=HR,DC=lab,DC=com" -Domain "lab"
   -DestPath "\\serwer\users" -LogPath "C:\ps_script\logs"
#>

[CmdletBinding()]
Param (
    [Parameter(Mandatory)]
    [string] $OUPath,

    [Parameter(Mandatory)]
    [string] $Domain,

    [Parameter(Mandatory)]
    [alias("DestPath")]
    [ValidateScript({ Test-Path $_ -PathType  "Container" })]
    [string] $path,

    
    [alias("Log")]
    [ValidateScript({ Test-Path $_ -PathType "Container"})]
    [string] $LogPath,

    #[Parameter]
    [string] $Admin = $false
)

function Get-LongDate {
    Get-Date -Format "yyyy-MM-dd_HH.mm.ss"
}
function Write-Log {
    param (
        [Parameter()]
        [string]$Type,
        [string]$Value
    )
    if($Type -eq "Err") {
        Write-Host -ForegroundColor Red "[ERROR] $(Get-LongDate) $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "$(Get-LongDate) [ERROR] > $Value"
        }
    }
    if($Type -eq "Succ") {
        Write-Host -ForegroundColor green "[SUCCESS] $(Get-LongDate) $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "$(Get-LongDate) [SUCCESS] > $Value"
        }
    }
    if($Type -eq "Info") {
        Write-Host "[INFO] $(Get-LongDate) $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "$(Get-LongDate) [INFO] > $Value"
        }
    }
}

function Test-Share {
    param (
        [Parameter()]
        [string] $Name
    )
    $share = (Get-SmbShare -Name $Name -ErrorAction SilentlyContinue).Name
    if($share -eq $Name){
        return $true
    }
    else {
        return $false
    }  
}

if($LogPath) {
    $LogDate = Get-LongDate
    $Log = "$LogPath\New-SRUsersShare_$LogDate.log"
    if(Test-Path -Path $Log) {
        Clear-Content -Path $Log
    }
    Add-Content -Path $Log -Encoding Ascii -Value "$(Get-LongDate) [INFO] > Log started"
}

<#Instalowanie modu�u#>
Install-Module -Name NTFSSecurity -Force  -WarningAction SilentlyContinue

<#Pobieranie kont u�ytkownik�w#>
try {
    $users = Get-ADUser -Filter "Enabled -eq 'True'" -SearchBase $OUPath | Select-Object -ExpandProperty SamAccountName
}
catch {
    Write-Log -Type 'Err' -Value "Not found OU: $OUPath"
}

foreach($user in $users) {

    Write-Host
    $directoryPath = "$path\$user"
    $account = "$Domain\$user"
    $shareTest

    if(Test-Path -Path $directoryPath) {
        Write-Log -Type 'Succ' -Value "[$user] Directory alredy exists."
    }
    else{
        New-Item -Name $user -Path $path -ItemType Directory | Out-Null
        Write-Log -Type 'Succ' -Value "[$user] Creating directory."

        if(-not (Test-Share -Name $user)){
            <#Tworzenie udziału#>
            $NSS = @{
                Name        = "$user"
                Path        = "$directoryPath"
                FullAccess  = "$account"
            }
            New-SmbShare @NSS | Out-Null
            Write-Log -Type 'Info' -Value "[$user] Creating share and add permission."

            <#Usuwanie dost�pu do udzia�u dla grupy Everyone#>
            $RSA = @{
                Name        = "$user"
                AccountName = "Wszyscy"
                Confirm     = $false
            }
            Write-Log -Type 'Info' -Value "[$user] Remove share permissions for a group: Everyone."
            Revoke-SmbShareAccess @RSA | Out-Null

            <#Wy��czenie dziedziczenia NTFS#>
            $DNI = @{
                Path                        = "$directoryPath"
                RemoveInheritedAccessRules  = $true
            }
            Write-Log -Type 'Info' -Value "[$user] Disable NTFS inheritance."
            Disable-NTFSAccessInheritance @DNI | Out-Null

            <#Nadawanie uprawnie� NTFS dla u�ytkownika#>
            $ANA1 = @{
                Path            = "$directoryPath"
                Account         = "$account"
                AccessRights    = 'FullControl'
            }
            Write-Log -Type "Info" -Value "[$user] Add permision Full Control for $user."
            Add-NTFSAccess @ANA1 | Out-Null

            <#Nadawanie uprawnie� NTFS dla systemu#>
            $ANA2 = @{
                Path            = "$directoryPath"
                Account         = "SYSTEM"
                AccessRights    = 'Full'
            }
            Write-Log -Type "Info" -Value "[$user] Add permision Full for SYSTEM."
            Add-NTFSAccess @ANA2 | Out-Null

            <#Nadawanie uprawnie� NTFS do odczytu dla administratora#>
            if($Admin) {
                $ANA3 = @{
                    Path            = "$directoryPath" 
                    Account         = "$Domain\$Admin"
                    AccessRights    = "Read"
                }
                Write-Log -Type "Info" -Value "[$user] Add permision Read for Administrator."
                Add-NTFSAccess @ANA3 | Out-Null
            }
        }
        else {
            Write-Log -Type 'Info' -Value "[$user] Share alredy exists."
        }
    } 
}
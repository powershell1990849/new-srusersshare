

# New-SRUsersShare.ps1

## SYNOPSIS
Skrypt do tworzenia udzia��w dla u�ytkownik�w na podstawie nazwy konta.

## SYNTAX

```
New-SRUsersShare.ps1 [-OUPath] <String> [-Domain] <String> [-path] <String> [[-LogPath] <String>]
 [[-Admin] <String>] [<CommonParameters>]
```

## DESCRIPTION
Problem: Utworzy� dla wszystkich u�ytkownik�w z AD udzia�� kt�rego nazwa b�dzie to�sama z nazw� konta u�ytkownika.

Skrypt pobiera nazw� konta u�ytkownika tworzy katalog odpowiadaj�cy tej nazwie, udost�pnia i nadaje uprawnienia.\
Dla konta 'jan.kowalski' utworzony zostanie katalog 'jan.kowalski' z uprawnieniami: \
NTFS - jan.kowalski/FullControl, SYSTEM/FullControl (ewentualnie Administrator/Read)\
Share - jan.kowalski/FullControl

Ka�demu u�ytkownikowi mo�na podpi�� jego uzia�� za pomoc� GPO: User Configuration -\> Preferences -\> Drive Maps -\> New Mapped Drive, w pole Location
podajemy \\\\serwer\�cie�ka do kotalogu z udzia�em\%LogonUser%

## EXAMPLES

### EXAMPLE 1
```
New-SRUsersShare.ps1 -OUPath "OU=Users,OU=HR,DC=lab,DC=com" -Domain "lab" -DestPath "\\serwer\users" -LogPath "C:\ps_script\logs"
```



## PARAMETERS

### -OUPath
�cie�ka do jednostki organizacyjnej AD w kt�rej znajduj� si� konta u�ytkownik�w (OU mog� by� zagnie�dzone).
Parametr wymagany.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Domain
Nazwa domeny (Pierwszy cz�on nazwy).
Parametr wymagany.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -DestPath
�cie�ka do katalogu w kt�rym utworzone zostan� udzia�y. Parametr wymagany.

```yaml
Type: String
Parameter Sets: (All)
Aliases: Path

Required: True
Position: 3
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -LogPath
�cie�ka do katalogu w kt�rym zapisany b�dzie log z wykonania skryptu. Parametr opcjonalny.

```yaml
Type: String
Parameter Sets: (All)
Aliases: Log

Required: False
Position: 4
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Admin
Parametr okre�laj�cy nazw� konta administratora, skrypt wywo�any z tym parametrem dodatkowo nada uprawnienia NTFS-ReadOnly
dla podanego konta administratora.
Parametr opcjonalny.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 5
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

 None.
## OUTPUTS

 Plik logu (New-SRUsersShare_2024-02-10_23.22.10.log) w formacie:\
 2024-02-10_23.22.10 [INFO] > Log started\
 2024-02-10_23.22.31 [SUCCESS] > [jan.kowalski] Creating directory.\
 2024-02-10_23.22.31 [INFO] > [jan.kowalski] Share alredy exists.\
 2024-02-10_23.22.31 [SUCCESS] > [anna.nowak] Creating directory.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Creating share and add permission.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Remove share permissions for a group: Everyone.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Disable NTFS inheritance.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Full Control for anna.nowak.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Full for SYSTEM.\
 2024-02-10_23.22.31 [INFO] > [anna.nowak] Add permision Read for Administrator.\
 ...
## NOTES
Version:        1.1\
Author:         Sebastian Cicho�ski\
Creation Date:  12.2023\
Projecturi:     https://gitlab.com/powershell1990849/new-srusersshare

## RELATED LINKS
